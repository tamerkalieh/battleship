<?php
/**
 * Created by PhpStorm.
 * User: Tamer
 * Date: 21.02.2019
 * Time: 13:36
 */

namespace App\Http\Controllers;

use phpDocumentor\Reflection\Types\Self_;

class BattleShip
{
    public $user_one = 'player one';
    public $user_two = 'player two';
    public $user_one_color = 'primary';
    public $user_two_color = 'danger';

    const NEW_SHIP = [
        'carrier' => 5,
        'battleship' => 4,
        'submarine' => 3,
        'cruiser' => 2,
        'patrol' => 1
    ];

    public function __construct(string $user_one, string $user_one_color, string $user_two, string $user_two_color)
    {
        $this->user_one = $user_one;
        $this->user_two = $user_two;
        $this->user_one_color = $user_one_color;
        $this->user_two_color = $user_two_color;
    }

    public function getGridWithShips()
    {
        $party = [
            $this->user_one => self::NEW_SHIP,
            $this->user_two => self::NEW_SHIP,
        ];
        return $this->chunkCoordinatesForPlayers($party, $this->getShipCoordinates($party));
    }

    public function placeSingleShip(int $num)
    {
        $ships = [
            1 => 'carrier',
            2 => 'battleship',
            3 => 'submarine',
            4 => 'cruiser',
            5 => 'patrol'
        ];
        $shipGrid = [
            'carrier' => [],
            'battleship' => [],
            'submarine' => [],
            'cruiser' => [],
            'patrol' => []
        ];
        $ship = $ships[$num];
        return $this->genCoordinates(self::NEW_SHIP[$ship], 5, $shipGrid);
    }

    private function getShipCoordinates(array $party)
    {
        $shipGrid = [
            'carrier' => [],
            'battleship' => [],
            'submarine' => [],
            'cruiser' => [],
            'patrol' => []
        ];

        foreach ($party as $user) {
            foreach ($user as $key => $ship) {
                $this->genCoordinates($ship, $key, $shipGrid);
            }
        }
        return $shipGrid;
    }

    private function chunkCoordinatesForPlayers(array $party, array $shipGrid)
    {
        foreach ($party as $user) {
            foreach ($user as $key => $ship) {
                $shipGrid[$key] = array_chunk($shipGrid[$key], count($shipGrid[$key]) / 2);
            }
        }
        return $shipGrid;
    }

    private function genCoordinates($ship, $key, $shipGrid)
    {
        $x = rand(1, 10);
        for ($i = 1; $i <= $ship; ++$i) {
            $y = $x;
            if (($x + $ship > 10)) $y -= $i;
            else $y += $i;
            $cord = [$x, $y];
            array_push($shipGrid[$key], $cord);
        }
    }
}
